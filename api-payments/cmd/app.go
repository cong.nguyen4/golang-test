package main

import (
	"api-payments/internal/configs"
	"api-payments/internal/db"
	"api-payments/internal/logs"
	"api-payments/internal/router"
	"api-payments/internal/shared"
	"fmt"

	"github.com/sirupsen/logrus"
)

func main() {
	config := configs.Load()
	logs.Setup(config)
	dbConntext := db.Connect(config)
	route := router.Setup(&shared.Dependencies{Config: config, DB: dbConntext.DB})

	route.Start(config.Server.Host + ":" + fmt.Sprint(config.Server.Port))
	logrus.Info(`Server start: ` + config.Server.Host + ":" + fmt.Sprint(config.Server.Port))
}
