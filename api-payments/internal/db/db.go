package db

import (
	"api-payments/internal/configs"
	"context"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type DBContext struct {
	Client  *mongo.Client
	DB      *mongo.Database
	Context context.Context
}

const (
	// Timeout operations after N seconds
	connectTimeout = 5
)

func Connect(config *configs.Config) *DBContext {
	db, client, ctx := getConnection(config.MongoDB.DBName, config.MongoDB.URI)
	return &DBContext{client, db, ctx}
}

func Disconnect(dbCtx *DBContext) {
	if err := dbCtx.Client.Disconnect(dbCtx.Context); err != nil {
		panic(errors.Wrap(err, "could not disconnect DB"))
	}
}

func getConnection(databaseName string, connectionURI string) (*mongo.Database, *mongo.Client, context.Context) {
	logrus.Info("Connect to db")

	client, err := mongo.NewClient(options.Client().ApplyURI(connectionURI))
	if err != nil {
		panic(errors.Wrap(err, "Failed to create client"))
	}

	ctx, _ := context.WithTimeout(context.Background(), connectTimeout*time.Second)

	err = client.Connect(ctx)
	if err != nil {
		panic(errors.Wrap(err, "Failed to connect to cluster"))
	}

	// Force a connection to verify our connection string
	err = client.Ping(ctx, nil)
	if err != nil {
		panic(errors.Wrap(err, "Failed to ping cluster"))
	}

	db := client.Database(databaseName)

	return db, client, ctx
}
