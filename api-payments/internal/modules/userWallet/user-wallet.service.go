package userWallet

import (
	"api-payments/internal/shared"
	"context"

	"go.mongodb.org/mongo-driver/mongo"
)

type UserWalletService struct {
	shared.BaseService
	PaymentModel *mongo.Collection
}

func (s *UserWalletService) ValidatePayment(ctx context.Context, userId string, amount float32) bool {
	return true
}
