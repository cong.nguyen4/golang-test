package payments

import (
	"api-payments/internal/externals/auth"
	"api-payments/internal/shared"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

type PaymentUserController struct {
	shared.BaseController
	PaymentService *PaymentService
}

// GetPaymentList godoc
// @Id getPaymentList
// @Summary Get Payment list
// @Description
// @Tags payment
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Param filter query FilterPaymentsDto true "Payment Filter"
// @Param pagination query shared.PaginationDto true "Payment Filter"
// @Response 200 {object} GetPaymentsReponse "Payment Info"
// @Router /user/payments [get]
func (controller *PaymentUserController) GetAll(ctx echo.Context) error {
	validate := validator.New()
	user, _ := ctx.Get("User").(auth.User)

	var dto FilterPaymentsDto
	var paginationDto shared.PaginationDto
	shared.BindAndValdiateQuery(ctx, &paginationDto, validate)
	shared.BindAndValdiateQuery(ctx, &dto, validate)
	ctx.JSON(http.StatusOK, controller.PaymentService.UserGetPayments(ctx.Request().Context(), user, dto, paginationDto))
	return nil
}

// GetPaymentDetails godoc
// @Id getPaymentDetails
// @Summary Get a payments details
// @Description
// @Tags payment
// @Accept json
// @Produce json
// @Param paymentId path string true "Payment ID"
// @Security ApiKeyAuth
// @Response 200 {object} PaymentsBase "Payment Info"
// @Router /user/payments/{paymentId} [get]
func (controller *PaymentUserController) GetOne(ctx echo.Context) error {
	user, _ := ctx.Get("User").(auth.User)
	ctx.JSON(http.StatusOK, controller.PaymentService.UserGetPaymentById(ctx.Request().Context(), ctx.Param("paymentId"), user))
	return nil
}
