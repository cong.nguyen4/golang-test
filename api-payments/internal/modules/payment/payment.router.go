package payments

import (
	"api-payments/internal/externals/order"
	"api-payments/internal/middlewares"
	"api-payments/internal/modules/userWallet"
	"api-payments/internal/shared"

	"github.com/labstack/echo/v4"
)

func SetupRouter(r *echo.Group, d *shared.Dependencies) {
	Dependencies := shared.Dependencies{
		Config: d.Config,
		DB:     d.DB,
	}
	PaymentService := &PaymentService{
		BaseService: shared.BaseService{
			Dependencies: Dependencies,
			Repository: shared.BaseRepository{
				Collection: d.DB.Collection("payments"),
			},
		},
		PaymentModel: d.DB.Collection("payments"),
		OrderServices: &order.OrderServices{
			Config: d.Config,
		},
		UserWallet: &userWallet.UserWalletService{
			BaseService: shared.BaseService{
				Dependencies: Dependencies,
				Repository: shared.BaseRepository{
					Collection: d.DB.Collection("userWallet"),
				},
			},
			PaymentModel: d.DB.Collection("userWallet"),
		},
	}
	userController := PaymentUserController{
		PaymentService: PaymentService,
	}

	backendController := PaymentBackendController{
		PaymentService: PaymentService,
	}

	userPayment := r.Group("/user/payments")
	{
		userPayment.Use(middlewares.Authenticate)
		userPayment.GET("", userController.GetAll, middlewares.PaginationMiddleware)
		userPayment.GET("/:paymentId", userController.GetOne)
	}

	backendPayment := r.Group("/backend/payments")
	{
		backendPayment.POST("", backendController.Create)
		backendPayment.PUT("/cancel", backendController.Cancel)
		backendPayment.GET("/:paymentId", backendController.GetOne)
	}

}
