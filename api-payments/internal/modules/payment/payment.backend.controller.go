package payments

import (
	"api-payments/internal/shared"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

type PaymentBackendController struct {
	shared.BaseController
	PaymentService *PaymentService
}

// CancelPayment godoc
// @Id cancelPayments
// @Summary Update an drder
// @Description
// @Tags payment
// @Accept json
// @Produce json
// @Param payment body CancelPaymentDto true "Payment Info"
// @Response 200 {object} PaymentsBase "Payment Info"
// @Router /backend/payments/cancel [put]
func (controller *PaymentBackendController) Cancel(ctx echo.Context) error {
	var dto CancelPaymentDto
	validate := validator.New()

	if err := ctx.Bind(&dto); err != nil {
		ctx.JSON(http.StatusBadRequest, shared.Json{"error": err.Error()})
		return nil
	}

	if err := validate.Struct(dto); err != nil {
		ctx.JSON(http.StatusBadRequest, shared.ErrorReponse{Code: http.StatusBadRequest, Msg: err.Error()})
		return nil
	}
	ctx.JSON(http.StatusOK, controller.PaymentService.CancelPayment(ctx.Request().Context(), dto))
	return nil
}

// GetPaymentDetails godoc
// @Id backendGetPaymentDetails
// @Summary Get an drder details
// @Description
// @Tags payment
// @Accept json
// @Produce json
// @Param paymentId path string true "Payment ID"
// @Response 200 {object} PaymentsBase "Payment Info"
// @Router /backend/payments/{paymentId} [get]
func (controller *PaymentBackendController) GetOne(ctx echo.Context) error {
	ctx.JSON(http.StatusOK, controller.PaymentService.GetPaymentById(ctx.Request().Context(), ctx.Param("paymentId")))
	return nil
}

// CreatePayment godoc
// @Id createPayments
// @Summary Create an drder
// @Description
// @Tags payment
// @Accept json
// @Produce json
// @Param payment body CreatePaymentDto true "Payment Info"
// @Response 200 {object} PaymentsBase "Payment Info"
// @Router /backend/payments [post]
func (controller *PaymentBackendController) Create(ctx echo.Context) error {
	var dto CreatePaymentDto
	validate := validator.New()

	if err := ctx.Bind(&dto); err != nil {
		ctx.JSON(http.StatusBadRequest, shared.Json{"error": err.Error()})
		return nil
	}

	if err := validate.Struct(dto); err != nil {
		ctx.JSON(http.StatusBadRequest, shared.ErrorReponse{Code: http.StatusBadRequest, Msg: err.Error()})
		return nil
	}

	ctx.JSON(http.StatusCreated, controller.PaymentService.CreatePayment(ctx.Request().Context(), dto))
	return nil
}
