package payments

import (
	"api-payments/internal/externals/auth"
	"api-payments/internal/externals/order"
	"api-payments/internal/modules/userWallet"
	"api-payments/internal/shared"
	"context"
	"math/rand"
	"net/http"
	"time"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type PaymentService struct {
	shared.BaseService
	OrderServices *order.OrderServices
	PaymentModel  *mongo.Collection
	UserWallet    *userWallet.UserWalletService
}

func (s *PaymentService) UserGetPayments(ctx context.Context, user auth.User, filterDto FilterPaymentsDto, paginationDto shared.PaginationDto) *GetPaymentsReponse {
	filter := make(map[string]interface{})
	filter["userId"] = user.UserId
	if filterDto.Status != "" {
		filter["status"] = filterDto.Status
	}
	if filterDto.OrderId != "" {
		filter["orderId"] = filterDto.OrderId
	}

	var payments = make([]PaymentsBase, 0)
	skip := int64((paginationDto.Page - 1) * paginationDto.PerPage)
	limit := int64(paginationDto.PerPage)
	opts := options.FindOptions{
		Limit: &limit,
		Skip:  &skip,
		Sort: map[string]interface{}{
			"createdAt": -1,
		},
	}
	s.Repository.Find(ctx, filter, &payments, &opts)
	total, err := s.Repository.Collection.CountDocuments(ctx, filter)
	if err != nil {
		panic(err)
	}
	return &GetPaymentsReponse{
		Data: payments,
		Metadata: shared.Metadata{
			PageSize:    int64(paginationDto.PerPage),
			Total:       total,
			CurrentPage: int64(paginationDto.Page),
		},
	}
}

func (s *PaymentService) GetPaymentById(ctx context.Context, id string) *PaymentsBase {
	filter := s.GetQueryFindById(id)
	var payment PaymentsBase

	s.Repository.FindOneOrFail(ctx, filter, &payment)
	return &payment
}

func (s *PaymentService) UserGetPaymentById(ctx context.Context, id string, user auth.User) *PaymentsBase {
	filter := s.GetQueryFindById(id)
	filter["userId"] = user.UserId
	var payment PaymentsBase
	s.Repository.FindOneOrFail(ctx, filter, &payment)
	s.OrderServices.CancelOrder(payment.OrderId)
	return &payment
}

func (s *PaymentService) CreatePayment(ctx context.Context, dto CreatePaymentDto) *PaymentsBase {
	var status PAYMENT_STATUS
	if !s.UserWallet.ValidatePayment(ctx, dto.UserId, dto.Amount) {
		status = PAYMENT_STATUS_CANCELLED
	} else {
		status = PAYMENT_STATUS_CREATED
	}
	paymentInfo := PaymentsBase{
		Id:              primitive.NewObjectID(),
		CreatedAt:       time.Now(),
		UpdatedAt:       time.Now(),
		UserId:          dto.UserId,
		OrderId:         dto.OrderId,
		IsNotifiedOrder: false,
		Status:          status,
		Amount:          dto.Amount,
	}

	_, err := s.PaymentModel.InsertOne(ctx, &paymentInfo)
	if err != nil {
		panic(errors.Wrap(err, "could not insert payment to DB"))
	}

	if status == PAYMENT_STATUS_CREATED {
		go s.ProcessPayment(paymentInfo)
	} else {
		go s.CancelPaymentAndNotifyOrder(context.TODO(), paymentInfo.Id.Hex(), paymentInfo.OrderId)
	}

	return &paymentInfo
}

func (s *PaymentService) CancelPaymentAndNotifyOrder(ctx context.Context, id string, orderId string) {
	s.CancelPayment(ctx, CancelPaymentDto{OrderId: orderId})
	if s.OrderServices.CancelOrder(orderId) {
		filter := map[string]interface{}{
			"orderId": orderId,
		}
		update := map[string]interface{}{
			"$set": map[string]interface{}{
				"isNotifiedOrder": true,
			},
		}
		s.Repository.UpdateOne(context.TODO(), filter, update, new(PaymentsBase))
	}
}

func (s *PaymentService) CancelPayment(ctx context.Context, dto CancelPaymentDto) *PaymentsBase {
	filter := map[string]interface{}{
		"orderId": dto.OrderId,
	}
	return s.UpdatePaymentStatus(ctx, PAYMENT_STATUS_CANCELLED, filter)
}

func (s *PaymentService) GetObjectIdFromString(id string) primitive.ObjectID {
	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		shared.ThrowException(&shared.ErrorReponse{Code: http.StatusBadRequest, Msg: "PaymentId must be ObjectId"})
	}

	return objId
}

func (s *PaymentService) ProcessPayment(transaction PaymentsBase) {
	if s.processPayment(transaction) {
		s.ConfirmPaymentAndNotifyOrder(context.TODO(), transaction.Id.Hex(), transaction.OrderId)
	} else {
		s.CancelPaymentAndNotifyOrder(context.TODO(), transaction.Id.Hex(), transaction.OrderId)
	}
}

func (s *PaymentService) processPayment(transaction PaymentsBase) bool {
	return s.handlePaymentLogic(transaction)
}

func (s *PaymentService) handlePaymentLogic(transaction PaymentsBase) bool {
	rand.Seed(time.Now().UTC().UnixNano())
	number := rand.Intn(3)
	return number%2 == 0
}

func (s *PaymentService) ConfirmPaymentAndNotifyOrder(ctx context.Context, id string, orderId string) {
	s.ConfirmPayment(ctx, id)
	if s.OrderServices.ConfirmOrder(orderId) {
		filter := s.GetQueryFindById(id)

		update := map[string]interface{}{
			"$set": map[string]interface{}{
				"isNotifiedOrder": true,
			},
		}
		s.Repository.UpdateOne(context.TODO(), filter, update, (new(PaymentsBase)))
	}
}

func (s *PaymentService) ConfirmPayment(ctx context.Context, id string) *PaymentsBase {
	filter := s.GetQueryFindById(id)
	return s.UpdatePaymentStatus(ctx, PAYMENT_STATUS_CONFIRMED, filter)
}

func (s *PaymentService) UpdatePaymentStatus(ctx context.Context, status PAYMENT_STATUS, filter map[string]interface{}) *PaymentsBase {
	update := map[string]interface{}{
		"$set": map[string]interface{}{
			"status": status,
		},
	}
	var updatedDocument PaymentsBase
	updateError := s.Repository.UpdateOne(ctx, filter, update, &updatedDocument)

	if updateError != nil {
		shared.ThrowException(&shared.ErrorReponse{Code: http.StatusNotFound, Msg: "Order not found"})
	}

	return &updatedDocument
}

func (s *PaymentService) GetObjectIdFromStringOrFail(id string) primitive.ObjectID {
	objId := shared.GetObjectIdFromString(id)

	if objId.IsZero() {
		shared.ThrowException(&shared.ErrorReponse{Code: http.StatusBadRequest, Msg: "OrderId must be ObjectId"})
	}

	return objId
}

func (s *PaymentService) GetQueryFindById(id string) map[string]interface{} {
	objId := s.GetObjectIdFromStringOrFail(id)
	return map[string]interface{}{
		"_id": objId,
	}
}
