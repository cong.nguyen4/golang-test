package payments

import "api-payments/internal/shared"

type CreatePaymentDto struct {
	Amount  float32 `json:"amount"`
	OrderId string  `json:"orderId"`
	UserId  string  `json:"userId"`
}

type CancelPaymentDto struct {
	OrderId string `json:"orderId"`
}

type FilterPaymentsDto struct {
	Status  PAYMENT_STATUS `json:"status,omitempty" query:"status" enums:"CREATED,CONFIRMED,CANCELLED,DELIVERED" validate:"omitempty,oneof=CREATED CONFIRMED CANCELLED"`
	OrderId string         `json:"orderId,omitempty" query:"orderId" validate:"omitempty"`
}

type GetPaymentsReponse struct {
	Data     []PaymentsBase  `json:"data"`
	Metadata shared.Metadata `json:"metadata"`
}
