package payments

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type PaymentsBase struct {
	Id              primitive.ObjectID `json:"id" bson:"_id"`
	OrderId         string             `json:"orderId" bson:"orderId"`
	Amount          float32            `json:"amount" bson:"amount"`
	Status          PAYMENT_STATUS     `json:"status" bson:"status"`
	CreatedAt       time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt       time.Time          `json:"updatedAt" bson:"updatedAt"`
	UserId          string             `json:"userId" bson:"userId"`
	IsNotifiedOrder bool               `json:"isNotifiedOrder" bson:"isNotifiedOrder"`
}
