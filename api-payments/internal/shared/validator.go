package shared

import (
	"net/http"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

func BindAndValdiateQuery(ctx echo.Context, dto interface{}, validate *validator.Validate) {
	if err := (&echo.DefaultBinder{}).BindQueryParams(ctx, dto); err != nil {
		ThrowException(
			&ErrorReponse{
				Code: http.StatusBadRequest,
				Msg:  err.Error(),
			},
		)
	}
	Valdiate(ctx, dto, validate)
}

func BindAndValdiateBody(ctx echo.Context, dto interface{}, validate *validator.Validate) {
	if err := ctx.Bind(dto); err != nil {
		ThrowException(
			&ErrorReponse{
				Code: http.StatusBadRequest,
				Msg:  err.Error(),
			},
		)
	}
	Valdiate(ctx, dto, validate)
}

func Valdiate(ctx echo.Context, dto interface{}, validate *validator.Validate) {
	if err := validate.Struct(dto); err != nil {
		ThrowException(
			&ErrorReponse{
				Code: http.StatusBadRequest,
				Msg:  err.Error(),
			},
		)
	}
}
