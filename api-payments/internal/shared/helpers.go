package shared

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetObjectIdFromString(id string) primitive.ObjectID {
	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return primitive.NilObjectID
	}

	return objId
}

func GetQueryFindById(objId primitive.ObjectID) map[string]interface{} {
	return map[string]interface{}{
		"_id": objId,
	}
}
