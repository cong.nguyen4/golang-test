package shared

import (
	"context"
	"fmt"
	"net/http"
	"reflect"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type BaseRepository struct {
	*mongo.Collection
}

func (s *BaseRepository) FindOneOrFailById(ctx context.Context, id primitive.ObjectID, result interface{}) {
	filter := GetQueryFindById(id)

	s.FindOneOrFail(ctx, filter, result)
}

func (s *BaseRepository) FindOneOrFail(ctx context.Context, filter map[string]interface{}, result interface{}, opts ...*options.FindOptions) {
	err := s.FindOne(ctx, filter, result, opts...)
	if err != nil {
		ThrowException(&ErrorReponse{Code: http.StatusNotFound, Msg: "Not found"})
	}
}

func (s *BaseRepository) FindOne(ctx context.Context, filter map[string]interface{}, result interface{}, opts ...*options.FindOptions) error {
	res := s.Collection.FindOne(ctx, filter)
	fmt.Println(reflect.TypeOf(result))

	err := res.Decode(result)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return err
		}
		panic(err)
	}

	return nil
}

func (s *BaseRepository) Find(ctx context.Context, filter map[string]interface{}, result interface{}, opts ...*options.FindOptions) {
	cur, err := s.Collection.Find(ctx, filter, opts...)
	if err != nil {
		panic(err)
	}
	if err = cur.All(ctx, result); err != nil {
		panic(err)
	}
}

func (s *BaseRepository) UpdateOne(ctx context.Context, filter map[string]interface{}, update map[string]interface{}, result interface{}, opts ...*options.FindOneAndUpdateOptions) error {
	after := options.After
	opt := options.FindOneAndUpdateOptions{
		ReturnDocument: &after,
	}
	opts = append(opts, &opt)
	updateError := s.FindOneAndUpdate(ctx, filter, update, opts...).Decode(result)

	if updateError != nil {
		if updateError == mongo.ErrNoDocuments {
			return updateError
		}
		panic(updateError)
	}
	return nil
}
