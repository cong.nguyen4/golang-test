package shared

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/sirupsen/logrus"
)

func ExecutePutRequest(url string, body interface{}) ([]byte, error) {
	var postBody *bytes.Buffer
	if body != nil {
		postBody = new(bytes.Buffer)
		json.NewEncoder(postBody).Encode(body)
	} else {
		postBody = nil
	}
	return ExecuteRequest(http.MethodPut, url, postBody)
}

func ExecutePostRequest(url string, body interface{}) ([]byte, error) {
	postBody := new(bytes.Buffer)
	json.NewEncoder(postBody).Encode(body)

	return ExecuteRequest(http.MethodPost, url, postBody)
}

func ExecuteRequest(method, url string, body *bytes.Buffer) ([]byte, error) {
	client := &http.Client{}

	var r *http.Request
	var err error
	if body == nil {
		r, err = http.NewRequest(method, url, nil)
	} else {
		r, err = http.NewRequest(method, url, body)
	}
	r.Header.Set("Content-Type", "application/json; charset=utf-8")

	resp, _ := client.Do(r)
	if err != nil {
		logrus.Error("An Error Occured %v", err)
		return nil, err
	}

	defer resp.Body.Close()
	resBody, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		logrus.Error("something went wrong with orderService", err)
		return nil, err
	}

	return resBody, nil
}
