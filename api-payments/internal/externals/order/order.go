package order

import (
	"api-payments/internal/configs"
	"api-payments/internal/shared"
	"fmt"

	"github.com/sirupsen/logrus"
)

type OrderServices struct {
	Config *configs.Config
}

func (orderServices *OrderServices) CancelOrder(orderId string) bool {
	logrus.Debug("Notify cancel order")
	orderUrl := "http://" + fmt.Sprint(orderServices.Config.Services.Order.Host) + ":" + fmt.Sprint(orderServices.Config.Services.Order.Port) + "/api/orders/backend/orders/" + orderId + "/cancel"
	_, err := shared.ExecutePutRequest(orderUrl, nil)
	return err == nil
}

func (orderServices *OrderServices) ConfirmOrder(orderId string) bool {
	logrus.Debug("Notify confirm order")

	orderUrl := "http://" + fmt.Sprint(orderServices.Config.Services.Order.Host) + ":" + fmt.Sprint(orderServices.Config.Services.Order.Port) + "/api/orders/backend/orders/" + orderId + "/confirm"
	_, err := shared.ExecutePutRequest(orderUrl, nil)
	return err == nil
}
