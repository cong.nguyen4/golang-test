package middlewares

import (
	"bytes"
	"io/ioutil"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

type bodyLogWriter struct {
	http.ResponseWriter
	body *bytes.Buffer
}

func (w bodyLogWriter) Write(b []byte) (int, error) {
	w.body.Write(b)
	return w.ResponseWriter.Write(b)
}

func RequestLogMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			var reqBody []byte
			if ctx.Request().Body != nil {
				reqBody, _ = ioutil.ReadAll(ctx.Request().Body)
			}
			ctx.Request().Body = ioutil.NopCloser(bytes.NewBuffer(reqBody))

			logrus.WithFields(logrus.Fields{
				"cid":    GetCorrelationID(ctx),
				"url":    ctx.Request().RequestURI,
				"method": ctx.Request().Method,
				"body":   string(reqBody),
				"header": ctx.Request().Header,
			}).Debug("Request")

			return next(ctx)
		}
	}
}
