package middlewares

import (
	"github.com/labstack/echo/v4"
)

func PaginationMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if c.QueryParam("page") == "" {
			c.QueryParams().Add("page", "1")
		}
		if c.QueryParam("perPage") == "" {
			c.QueryParams().Add("perPage", "20")
		}
		return next(c)
	}
}
