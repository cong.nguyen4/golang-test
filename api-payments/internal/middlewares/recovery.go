package middlewares

import (
	"api-payments/internal/shared"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

func RecoveryMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			defer func() {
				if r := recover(); r != nil {
					if err, ok := r.(shared.ErrorReponse); ok {
						c.JSON(err.Code, err)
						return
					}
					err, ok := r.(error)
					if !ok {
						err = fmt.Errorf("%v", r)
					}

					c.JSON(http.StatusInternalServerError, shared.ErrorReponse{
						Code: 500, Msg: fmt.Sprintf("%s", err.Error()),
					})
					c.Error(err)
				}
			}()
			return next(c)
		}
	}
}
