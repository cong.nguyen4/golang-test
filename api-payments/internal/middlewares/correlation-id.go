package middlewares

import (
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

const (
	CTX_KEY_CORRELATION_ID = "CorrelationID"
)

func CorrelationIDMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if GetCorrelationID(c) == "" {
			uid, _ := uuid.NewRandom()
			c.Set(CTX_KEY_CORRELATION_ID, uid.String())
		}
		return next(c)
	}
}

func GetCorrelationID(ctx echo.Context) string {
	corID, ok := ctx.Get(CTX_KEY_CORRELATION_ID).(string)
	if !ok {
		return ""
	}
	return corID
}
