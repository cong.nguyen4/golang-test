package main

import (
	"api-orders/internal/configs"
	"api-orders/internal/db"
	"api-orders/internal/logs"
	"api-orders/internal/router"
	"api-orders/internal/shared"
	"fmt"

	"github.com/sirupsen/logrus"
)

func main() {
	config := configs.Load()
	logs.Setup(config)
	dbConntext := db.Connect(config)
	route := router.Setup(&shared.Dependencies{Config: config, DB: dbConntext.DB})

	route.Start(config.Server.Host + ":" + fmt.Sprint(config.Server.Port))
	logrus.Info(`Server start: ` + config.Server.Host + ":" + fmt.Sprint(config.Server.Port))
}
