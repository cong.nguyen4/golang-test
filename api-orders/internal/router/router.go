package router

import (
	"api-orders/internal/middlewares"
	"api-orders/internal/modules/orders"
	"api-orders/internal/shared"
	"api-orders/internal/swagger"
	"encoding/json"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
)

func Setup(d *shared.Dependencies) *echo.Echo {
	e := echo.New()
	if d.Config.Service.DocEnabled {
		swagger.Setup(e, d)
	}

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowHeaders:  strings.Split(d.Config.Server.Cors.AllowedHeaders, ","),
		ExposeHeaders: strings.Split(d.Config.Server.Cors.ExposedHeaders, ","),
	}))

	api := e.Group(d.Config.Service.APIBasePath)
	{
		api.Use(middlewares.CorrelationIDMiddleware)
		api.Use(middleware.BodyDump(func(ctx echo.Context, reqBody, resBody []byte) {
			var res interface{}
			json.Unmarshal([]byte(resBody), &res)
			logrus.WithFields(logrus.Fields{
				"cid":        middlewares.GetCorrelationID(ctx),
				"statusCode": ctx.Response().Status,
				"body":       res,
				"header":     ctx.Response().Writer.Header(),
			}).Debug("Response")
		}))
		api.Use(middlewares.RecoveryMiddleware())
		api.Use(middlewares.RequestLogMiddleware())

		orders.SetupRouter(api, d)
	}

	return e
}
