package middlewares

import (
	"api-orders/internal/externals/auth"
	"api-orders/internal/shared"

	"github.com/labstack/echo/v4"
)

const ACCESS_KEY = "Access-Token"

func responseWithError(c echo.Context, code int, message string) {
	c.JSON(code, shared.ErrorReponse{
		Code: code,
		Msg:  message,
	})
}

// Authenticate fetches user details from token
func Authenticate(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		requiredToken := c.Request().Header[ACCESS_KEY]

		if len(requiredToken) == 0 {
			responseWithError(c, 403, "Please login to your account")
			return nil
		}

		userID, _ := shared.DecodeToken(requiredToken[0])

		result, err := auth.GetAndValidateUser(userID)

		if result.UserId == "" {
			responseWithError(c, 404, "User account not found")
			return nil
		}

		if err != nil {
			responseWithError(c, 500, "Something went wrong giving you access")
			return nil
		}

		c.Set("User", result)

		return next(c)

	}
}
