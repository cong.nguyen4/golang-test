package shared

import (
	"api-orders/internal/configs"

	"go.mongodb.org/mongo-driver/mongo"
)

type Dependencies struct {
	Config *configs.Config
	DB     *mongo.Database
}

type BaseController struct {
}

type BaseService struct {
	Dependencies
	Repository BaseRepository
}

type ErrorReponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

type PaginationDto struct {
	PerPage int `json:"perPage,omitempty" query:"perPage" validate:"omitempty,number,gt=0"`
	Page    int `json:"page,omitempty" query:"page" validate:"omitempty,number,gt=0"`
}

type Metadata struct {
	CurrentPage int64 `json:"currentPage"`
	PageSize    int64 `json:"pageSize"`
	Total       int64 `json:"total"`
}

type Json map[string]interface{}

func ThrowException(err *ErrorReponse) {
	panic(*err)
}
