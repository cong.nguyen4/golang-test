package shared

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/sirupsen/logrus"
)

func ExecutePutRequest(url string, body interface{}) ([]byte, error) {
	postBody := new(bytes.Buffer)
	json.NewEncoder(postBody).Encode(body)

	return ExecuteRequest(http.MethodPut, url, postBody)
}

func ExecutePostRequest(url string, body interface{}) ([]byte, error) {
	postBody := new(bytes.Buffer)
	json.NewEncoder(postBody).Encode(body)

	return ExecuteRequest(http.MethodPost, url, postBody)
}

func ExecuteRequest(method, url string, body io.Reader) ([]byte, error) {
	client := &http.Client{}

	r, err := http.NewRequest(method, url, body)
	r.Header.Set("Content-Type", "application/json; charset=utf-8")

	resp, _ := client.Do(r)
	if err != nil {
		logrus.Error("An Error Occured %v", err)
		return nil, err
	}
	defer resp.Body.Close()
	resBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.Error("something went wrong with orderService", err)
		return nil, err
	}

	return resBody, nil
}
