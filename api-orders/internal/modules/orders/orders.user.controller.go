package orders

import (
	"api-orders/internal/externals/auth"
	"api-orders/internal/shared"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

type OrderUserController struct {
	shared.BaseController
	OrderService *OrderService
}

// GetOrderList godoc
// @Id getOrderList
// @Summary Get order list
// @Description
// @Tags order
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Response 200 {object} GetOrdersReponse "Order Info"
// @Param filter query FilterOrdersDto true "Order Filter"
// @Param pagination query shared.PaginationDto true "Order Filter"
// @Router /user/orders [get]
func (controller *OrderUserController) GetAll(ctx echo.Context) error {
	validate := validator.New()
	user, _ := ctx.Get("User").(auth.User)

	var dto FilterOrdersDto
	var paginationDto shared.PaginationDto
	shared.BindAndValdiateQuery(ctx, &paginationDto, validate)
	shared.BindAndValdiateQuery(ctx, &dto, validate)

	ctx.JSON(http.StatusOK, controller.OrderService.UserGetOrders(ctx.Request().Context(), user, dto, paginationDto))
	return nil
}

// GetOrderDetails godoc
// @Id getOrderDetails
// @Summary Get an order details
// @Description
// @Tags order
// @Accept json
// @Produce json
// @Param orderId path string true "Order ID"
// @Security ApiKeyAuth
// @Response 200 {object} OrdersBase "Order Info"
// @Router /user/orders/{orderId} [get]
func (controller *OrderUserController) GetOne(ctx echo.Context) error {
	user, _ := ctx.Get("User").(auth.User)
	ctx.JSON(http.StatusOK, controller.OrderService.UserGetOrderById(ctx.Request().Context(), ctx.Param("orderId"), user))
	return nil
}

// CreateOrder godoc
// @Id createOrders
// @Summary Create an order
// @Description
// @Tags order
// @Accept json
// @Produce json
// @Param order body OrderCreateDto true "Order Info"
// @Response 200 {object} OrdersBase "Order Info"
// @Security ApiKeyAuth
// @Router /user/orders [post]
func (controller *OrderUserController) Create(ctx echo.Context) error {
	var dto OrderCreateDto
	shared.BindAndValdiateBody(ctx, &dto, validator.New())

	user, _ := ctx.Get("User").(auth.User)
	ctx.JSON(http.StatusCreated, controller.OrderService.CreateOrder(ctx.Request().Context(), dto, user))
	return nil
}

// CancelOrder godoc
// @Id cancelOrders
// @Summary Update an order
// @Description
// @Tags order
// @Accept json
// @Produce json
// @Param orderId path string true "Order ID"
// @Response 200 {object} OrdersBase "Order Info"
// @Security ApiKeyAuth
// @Router /user/orders/{orderId}/cancel [put]
func (controller *OrderUserController) Cancel(ctx echo.Context) error {
	user, _ := ctx.Get("User").(auth.User)
	ctx.JSON(http.StatusOK, controller.OrderService.UserCancelOrder(ctx.Request().Context(), ctx.Param("orderId"), user))
	return nil
}
