package orders

import (
	"api-orders/internal/externals/auth"
	"api-orders/internal/externals/payment"
	"api-orders/internal/shared"
	"context"
	"net/http"
	"time"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type OrderService struct {
	shared.BaseService
	PaymentServices *payment.PaymentServices
}

func (s *OrderService) UserGetOrders(ctx context.Context, user auth.User, filterDto FilterOrdersDto, paginationDto shared.PaginationDto) GetOrdersReponse {
	filter := make(map[string]interface{})
	filter["userId"] = user.UserId

	if filterDto.Status != "" {
		filter["status"] = filterDto.Status
	}

	var orders = make([]OrdersBase, 0)
	skip := int64((paginationDto.Page - 1) * paginationDto.PerPage)
	limit := int64(paginationDto.PerPage)
	opts := options.FindOptions{
		Limit: &limit,
		Skip:  &skip,
		Sort: map[string]interface{}{
			"createdAt": -1,
		},
	}
	s.Repository.Find(ctx, filter, &orders, &opts)
	total, err := s.Repository.Collection.CountDocuments(ctx, filter)
	if err != nil {
		panic(err)
	}
	return GetOrdersReponse{
		Data: orders,
		Metadata: shared.Metadata{
			PageSize:    int64(paginationDto.PerPage),
			Total:       total,
			CurrentPage: int64(paginationDto.Page),
		},
	}
}

func (s *OrderService) UserGetOrderById(ctx context.Context, id string, user auth.User) *OrdersBase {
	filter := s.GetQueryFindById(id)
	filter["userId"] = user.UserId
	var order OrdersBase
	s.Repository.FindOneOrFail(ctx, filter, &order)
	return &order
}

func (s *OrderService) CreateOrder(ctx context.Context, dto OrderCreateDto, user auth.User) *OrdersBase {
	if !user.IsVerified {
		shared.ThrowException(&shared.ErrorReponse{
			Code: http.StatusBadRequest,
			Msg:  "Cannot create order if user has not been verified account",
		})
	}
	orderInfo := OrdersBase{
		Id:              primitive.NewObjectID(),
		Products:        dto.Products,
		CreatedAt:       time.Now(),
		UpdatedAt:       time.Now(),
		UserId:          user.UserId,
		NotifyToPayment: false,
		Status:          ORDER_STATUS_CREATED,
	}

	_, err := s.BaseService.Repository.InsertOne(ctx, &orderInfo)
	if err != nil {
		panic(errors.Wrap(err, "could not insert order to DB"))
	}

	go s.SendOrderToPayment(&orderInfo)

	return &orderInfo
}

func (s *OrderService) BackendCancelOrder(ctx context.Context, id string) *OrdersBase {
	return s.CancelOrder(ctx, id)
}

func (s *OrderService) BackendConfirmOrder(ctx context.Context, id string) *OrdersBase {
	return s.ConfirmOrder(ctx, id)
}

func (s *OrderService) UserCancelOrder(ctx context.Context, id string, user auth.User) *OrdersBase {
	s.UserGetOrderById(ctx, id, user)

	order := s.CancelOrder(ctx, id)
	s.PaymentServices.CancelOrder(payment.CancelPaymentDto{OrderId: id})
	return order
}

func (s *OrderService) ConfirmOrder(ctx context.Context, id string) *OrdersBase {
	filter := s.GetQueryFindById(id)
	var order OrdersBase
	s.Repository.FindOneOrFail(ctx, filter, &order)

	if order.Status == ORDER_STATUS_DELIVERED {
		shared.ThrowException(&shared.ErrorReponse{
			Code: http.StatusBadRequest,
			Msg:  "Can not confirm a delivered order",
		})
	}

	if order.Status == ORDER_STATUS_CONFIRMED {
		return &order
	}

	return s.UpdateOrderStatus(ctx, ORDER_STATUS_CONFIRMED, filter)
}

func (s *OrderService) CancelOrder(ctx context.Context, id string) *OrdersBase {
	filter := s.GetQueryFindById(id)
	var order OrdersBase
	s.Repository.FindOneOrFail(ctx, filter, &order)

	if order.Status == ORDER_STATUS_DELIVERED {
		shared.ThrowException(&shared.ErrorReponse{
			Code: http.StatusBadRequest,
			Msg:  "Can not cancel a delivered order",
		})
	}

	if order.Status == ORDER_STATUS_CANCELLED {
		return &order
	}

	return s.UpdateOrderStatus(ctx, ORDER_STATUS_CANCELLED, filter)
}

func (s *OrderService) UpdateOrderStatus(ctx context.Context, status ORDER_STATUS, filter map[string]interface{}) *OrdersBase {
	update := map[string]interface{}{
		"$set": map[string]interface{}{
			"status": status,
		},
	}
	var updatedDocument OrdersBase
	updateError := s.Repository.UpdateOne(ctx, filter, update, &updatedDocument)

	if updateError != nil {
		shared.ThrowException(&shared.ErrorReponse{Code: http.StatusNotFound, Msg: "Order not found"})
	}

	return &updatedDocument
}

func (s *OrderService) SendOrderToPayment(order *OrdersBase) {
	var amount float32
	for _, v := range order.Products {
		amount += v.Price * float32(v.Amount)
	}
	err := s.PaymentServices.SendOrderToPayment(payment.CreatePaymentDto{
		Amount:  amount,
		OrderId: order.Id.Hex(),
		UserId:  order.UserId,
	})

	if err == nil {
		filter := s.GetQueryFindById(order.Id.Hex())
		update := map[string]interface{}{
			"$set": map[string]interface{}{
				"notifyToPayment": true,
			},
		}
		s.Repository.UpdateOne(context.TODO(), filter, update, new(OrdersBase))
	}
}

func (s *OrderService) GetObjectIdFromStringOrFail(id string) primitive.ObjectID {
	objId := shared.GetObjectIdFromString(id)

	if objId.IsZero() {
		shared.ThrowException(&shared.ErrorReponse{Code: http.StatusBadRequest, Msg: "OrderId must be ObjectId"})
	}

	return objId
}

func (s *OrderService) GetQueryFindById(id string) map[string]interface{} {
	objId := s.GetObjectIdFromStringOrFail(id)
	return map[string]interface{}{
		"_id": objId,
	}
}
