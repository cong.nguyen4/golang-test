package orders

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type OrdersBase struct {
	Id              primitive.ObjectID `json:"id" bson:"_id"`
	Products        []Product          `json:"products" bson:"products"`
	Status          ORDER_STATUS       `json:"status" bson:"status"`
	CreatedAt       time.Time          `json:"createdAt" bson:"createdAt"`
	NotifyToPayment bool               `json:"notifyToPayment" bson:"notifyToPayment"`
	UpdatedAt       time.Time          `json:"updatedAt" bson:"updatedAt"`
	UserId          string             `json:"userId" bson:"userId"`
}

type Product struct {
	Name   string  `json:"name" bson:"name"`
	Amount int     `json:"amount" bson:"amount"`
	Price  float32 `json:"price" bson:"price"`
}
