package orders

import "api-orders/internal/shared"

type OrderCreateDto struct {
	Products []Product `json:"products" validate:"required,dive,required"`
}

type FilterOrdersDto struct {
	Status ORDER_STATUS `json:"status,omitempty" query:"status" enums:"CREATED,CONFIRMED,CANCELLED,DELIVERED" validate:"omitempty,oneof=CREATED CONFIRMED CANCELLED DELIVERED"`
}

type GetOrdersReponse struct {
	Data     []OrdersBase    `json:"data"`
	Metadata shared.Metadata `json:"metadata"`
}
