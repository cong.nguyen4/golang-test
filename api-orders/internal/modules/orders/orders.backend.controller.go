package orders

import (
	"api-orders/internal/externals/auth"
	"api-orders/internal/shared"
	"net/http"

	"github.com/labstack/echo/v4"
)

type OrderBackendController struct {
	shared.BaseController
	OrderService *OrderService
}

// GetOrderDetails godoc
// @Id backendGetOrderDetails
// @Summary Get an drder details
// @Description
// @Tags order
// @Accept json
// @Produce json
// @Param orderId path string true "Order ID"
// @Response 200 {object} OrdersBase "Order Info"
// @Router /backend/orders/{orderId} [get]
func (controller *OrderBackendController) GetOne(ctx echo.Context) error {
	user, _ := ctx.Get("User").(auth.User)
	ctx.JSON(http.StatusOK, controller.OrderService.UserGetOrderById(ctx.Request().Context(), ctx.Param("orderId"), user))
	return nil
}

// CancelOrder godoc
// @Id backendCancelOrders
// @Summary Update an drder
// @Description
// @Tags order
// @Accept json
// @Produce json
// @Param orderId path string true "Order ID"
// @Response 200 {object} OrdersBase "Order Info"
// @Router /backend/orders/{orderId}/cancel [put]
func (controller *OrderBackendController) Cancel(ctx echo.Context) error {
	ctx.JSON(http.StatusOK, controller.OrderService.CancelOrder(ctx.Request().Context(), ctx.Param("orderId")))
	return nil
}

// ConfirmOrder godoc
// @Id backedConfirmlOrders
// @Summary Update an drder
// @Description
// @Tags order
// @Accept json
// @Produce json
// @Param orderId path string true "Order ID"
// @Response 200 {object} OrdersBase "Order Info"
// @Router /backend/orders/{orderId}/confirm [put]
func (controller *OrderBackendController) Confirm(ctx echo.Context) error {
	ctx.JSON(http.StatusOK, controller.OrderService.BackendConfirmOrder(ctx.Request().Context(), ctx.Param("orderId")))
	return nil
}
