package orders

import (
	"api-orders/internal/externals/payment"
	"api-orders/internal/middlewares"
	"api-orders/internal/shared"

	"github.com/labstack/echo/v4"
)

func SetupRouter(r *echo.Group, d *shared.Dependencies) {
	OrderRepository := shared.BaseRepository{
		Collection: d.DB.Collection("orders"),
	}
	Dependencies := shared.Dependencies{
		Config: d.Config,
		DB:     d.DB,
	}
	OrderService := &OrderService{
		BaseService: shared.BaseService{
			Dependencies: Dependencies,
			Repository:   OrderRepository,
		},
		PaymentServices: &payment.PaymentServices{
			Config: d.Config,
		},
	}
	userController := OrderUserController{
		OrderService: OrderService,
	}

	backendController := OrderBackendController{
		OrderService: OrderService,
	}
	be := r.Group("/backend/orders")
	{
		be.GET("/:orderId", backendController.GetOne)
		be.PUT("/:orderId/cancel", backendController.Cancel)
		be.PUT("/:orderId/confirm", backendController.Confirm)
	}

	rg := r.Group("/user/orders")
	{
		rg.Use(middlewares.Authenticate)
		rg.GET("", userController.GetAll, middlewares.PaginationMiddleware)
		rg.GET("/:orderId", userController.GetOne)
		rg.POST("", userController.Create)
		rg.PUT("/:orderId/cancel", userController.Cancel)
	}
}
