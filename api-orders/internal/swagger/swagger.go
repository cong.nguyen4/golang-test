package swagger

import (
	"api-orders/docs"
	"api-orders/internal/shared"

	"github.com/labstack/echo/v4"
	echoSwagger "github.com/swaggo/echo-swagger"
)

// @title Orders API
// @version 1.0
// @description Manage orders
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Access-token
func Setup(r *echo.Echo, d *shared.Dependencies) {
	docs.SwaggerInfo.Host = d.Config.ServiceBaseURL()
	docs.SwaggerInfo.BasePath = d.Config.Service.APIBasePath
	jsonURL := "http://" + d.Config.ServiceBaseURL() + d.Config.Service.DocBasePath + "/doc.json"
	r.GET(d.Config.Service.DocBasePath+"/*any", echoSwagger.EchoWrapHandler(echoSwagger.URL(jsonURL)))
}
