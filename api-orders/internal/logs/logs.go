package logs

import (
	"api-orders/internal/configs"
	"os"

	"github.com/sirupsen/logrus"
)

func Setup(config *configs.Config) {
	if config.ENV == "local" {
		logrus.SetFormatter(&logrus.TextFormatter{})
	} else {
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.DebugLevel)

	logrus.WithFields(logrus.Fields{"config": config}).Debug("Config")
}
