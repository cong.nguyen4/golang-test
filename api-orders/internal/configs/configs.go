package configs

import (
	"fmt"
	"os"
	"strings"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var viperConfig *viper.Viper
var config Config

type Config struct {
	ENV     string `env:"APP_ENV" env-default:"local" env:"ENV"`
	Service struct {
		Name                  string `mapstructure:"name" env:"SERVICE_NAME"`
		Description           string `mapstructure:"description" env:"SERVICE_DESCRIPTION"`
		APIBasePath           string `mapstructure:"baseUrl" env:"SERVICE_BASE_URL"`
		DocBasePath           string `mapstructure:"docBaseUrl" env:"SERVICE_DOC_BASE_URL"`
		DocEnabled            bool   `mapstructure:"docEnabled" env:"SERVICE_DOC_ENABLED"`
		InternalServicesRegex string `mapstructure:"internalServicesRegex" env:"SERVICE_GRACEFUL_SHUTDOWN_TIME"`
	} `mapstructure:"service"`
	Server struct {
		Host string `mapstructure:"host" env:"SERVER_HOST"`
		Port int    `mapstructure:"port" env:"SERVER_PORT"`
		Cors struct {
			AllowedHeaders string `mapstructure:"allowedHeaders" env:"SERVER_CORS_ALLOWED_HEADERS"`
			ExposedHeaders string `mapstructure:"exposedHeaders" env:"SERVER_CORS_EXPOSED_HEADERS"`
		} `mapstructure:"cors"`
	} `mapstructure:"server"`
	MongoDB struct {
		URI       string `mapstructure:"uri" env:"MONGODB_URI"`
		DBName    string `mapstructure:"dbName" env:"MONGODB_DBNAME"`
		Debug     bool   `mapstructure:"debug" env:"MONGODB_DEBUG"`
		MaxTimeMS int    `mapstructure:"maxTimeMS" env:"MONGODB_MAX_TIME_MS"`
	} `mapstructure:"mongodb"`
	Logger struct {
		Enabled  bool   `mapstructure:"enabled" env:"LOGGER_ENABLED"`
		Level    string `mapstructure:"level" env:"LOGGER_LEVEL"`
		Hostname string `mapstructure:"hostname" env:"LOGGER_HOST_NAME"`
		Port     int    `mapstructure:"port" env:"LOGGER_PORT"`
	} `mapstructure:"logger"`
	Services struct {
		Payment struct {
			Host string `mapstructure:"host" env:"SERVICES_PAYMENT_HOST"`
			Port int    `mapstructure:"port" env:"SERVICES_PAYMENT_PORT"`
		} `mapstructure:"payment"`
	} `mapstructure:"services"`
}

func (config *Config) ServiceBaseURL() string {
	return fmt.Sprintf("%s:%d", config.Server.Host, config.Server.Port)
}

func Load() *Config {
	logrus.Info("Loading config...")

	viperConfig = viper.New()

	// load config from default config file
	viperConfig.SetConfigFile("./configs/default.yaml")
	err := viperConfig.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	viperConfig.ReadInConfig()

	// load config from .env file if file exist
	if _, err := os.Stat("./.env"); err == nil {
		envViperConfig := viper.New()
		envViperConfig.SetConfigFile("./.env")
		err := envViperConfig.ReadInConfig()
		if err != nil {
			logrus.Warn(errors.Wrap(err, "Unable to read .env file, %v"))
		} else {
			for _, key := range envViperConfig.AllKeys() {
				newKey := strings.Replace(key, "_", ".", -1)
				viperConfig.Set(newKey, envViperConfig.Get(key))
			}
		}
	} else {
		logrus.Warn(errors.Wrap(err, "No .env file, %v"))
	}

	// sync env variables
	viperConfig.SetEnvPrefix("")
	viperConfig.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viperConfig.AutomaticEnv()

	err = viperConfig.Unmarshal(&config)
	if err != nil {
		panic(errors.Wrap(err, "Unable to decode into struct, %v"))
	}

	return &config
}
