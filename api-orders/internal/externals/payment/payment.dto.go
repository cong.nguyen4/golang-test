package payment

type CreatePaymentDto struct {
	Amount  float32 `json:"amount"`
	OrderId string  `json:"orderId"`
	UserId  string  `json:"userId"`
}

type CancelPaymentDto struct {
	OrderId string `json:"orderId"`
}
