package payment

import (
	"api-orders/internal/configs"
	"api-orders/internal/shared"
	"fmt"

	"github.com/sirupsen/logrus"
)

type PaymentServices struct {
	Config *configs.Config
}

func (paymentServices *PaymentServices) SendOrderToPayment(input CreatePaymentDto) error {
	logrus.Debug("Send order to payment")
	paymentUrl := fmt.Sprint("http://"+paymentServices.Config.Services.Payment.Host) + ":" + fmt.Sprint(paymentServices.Config.Services.Payment.Port) + "/api/payments/backend/payments"
	_, err := shared.ExecutePostRequest(paymentUrl, input)
	return err
}

func (paymentServices *PaymentServices) CancelOrder(input CancelPaymentDto) bool {
	logrus.Debug("Send cancel order request to payment")
	paymentCancelUrl := fmt.Sprint("http://"+paymentServices.Config.Services.Payment.Host) + ":" + fmt.Sprint(paymentServices.Config.Services.Payment.Port) + "/api/payments/backend/payments/cancel"
	_, err := shared.ExecutePutRequest(paymentCancelUrl, input)
	return err == nil
}
